/**
 * Created by Yuri on 01/08/2017.
 */

var app = angular.module('QuestionnairesApp', []);

app.directive('questionnaire', function () {
    return {
        restrict: 'E',
        scope: {},
        templateUrl: './template.html',
        controller: 'questionnaireController'
    }
});

app.controller('questionnaireController', ['$scope', '$http', function ($scope, $http) {
    $http.get('questionnaire.json').success(function (data) {
        $scope.questions = data;
    });

    $scope.settings = {};
    $scope.settings.quizHide = false;
    $scope.settings.activeQuestion = 0;

    $scope.answers = [
        {
            "key": null,
            "text": ""
        },
        {
            "key": null,
            "text": ""
        },
        {
            "key": null,
            "text": ""
        },
        {
            "key": null,
            "text": ""
        }
    ];

    //SELECTED
    $scope.onSelect = function (option) {
        $scope.answers[$scope.settings.activeQuestion].key = option.key;
        $scope.answers[$scope.settings.activeQuestion].text = option.text;

        // console.log($scope.settings.activeQuestion);
        if (option.next) {
            console.log("Catched!");
            $scope.settings.activeQuestion = option.next - 1;
        } else         if ($scope.settings.activeQuestion < $scope.questions.length - 1) {
            $scope.settings.activeQuestion++;
        }
    };

    //SUBMIT ANSWERS
    $scope.submitAnswers = function () {
        $scope.settings.quizHide = true;
        console.log($scope.settings.quizHide);


        var hiddenDiv = document.getElementById('hide');
        hiddenDiv.style.display = 'none';
        // hiddenDiv.style.transition= '1s';

        var ShownDiv = document.getElementById('show');
        ShownDiv.style.display = 'block';
        // ShownDiv.style.transition = '1s';

    };

    //NEXT QUESTION
    $scope.nextQuestion = function () {
        if ($scope.settings.activeQuestion < $scope.questions.length - 1) {
            $scope.settings.activeQuestion++;
        }


    };
    // PREVIOUS QUESTION
    $scope.previousQuestion = function () {
        if ($scope.settings.activeQuestion > 0) {
            $scope.settings.activeQuestion--;
        }
    }

}]);

